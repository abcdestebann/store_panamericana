import { SET_DATA_USER } from './actionTypes';

/**
 *
 *
 * @export
 * @param {*} data
 * @returns
 */
export function setDataUser(data) {
   return {
      type: SET_DATA_USER,
      payload: data
   }
}