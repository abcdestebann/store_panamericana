import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
   card: {
      minWidth: 200,
      marginRight: 10,
   },
   media: {
      height: 140,
      backgroundSize: 'contain',
      margin: 10,
   },
   boxInfo: {
      display: 'flex',
      alignItems: 'center',
   },
   value: {
      marginLeft: 5,
      fontSize: 12
   }
};


const CardProduct = ({
   product,
   handleToogleModal,
   category,
   index,
   handleShowProduct,
   classes,
}) => {
   return (
      <Card className={classes.card}>
         <CardMedia
            className={classes.media}
            image={product.image}
            title={product.name}
         />
         <CardContent>
            <Typography gutterBottom variant="h6" component="h5">
               {product.name}
            </Typography>
            <div className={classes.boxInfo}>
               <Typography variant="subtitle2" component="span"> Categoría: </Typography>
               <span className={classes.value}> {product.category}</span>
            </div>
            <div className={classes.boxInfo}>
               <Typography variant="subtitle2" component="span"> Sección: </Typography>
               <span className={classes.value}> {product.section}</span>
            </div>
            <div className={classes.boxInfo}>
               <Typography variant="subtitle2" component="span"> Valor: </Typography>
               <span className={classes.value}> $ {product.value.toLocaleString()}</span>
            </div>
            <div className={classes.boxInfo}>
               <Typography variant="subtitle2" component="span"> Cantidad: </Typography>
               <span className={classes.value}> {product.quantityEntry}</span>
            </div>
         </CardContent>
         <CardActions>
            <Button size="small" color="primary" onClick={() => handleShowProduct(product, index, category)}> Ver producto </Button>
            <Button size="small" color="secondary" onClick={() => handleToogleModal(true, product, index, category)}> Remover </Button>
         </CardActions>
      </Card>
   )
}

export default withStyles(styles)(CardProduct) 
