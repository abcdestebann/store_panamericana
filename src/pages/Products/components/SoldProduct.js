import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

const styles = {
   actions: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
   },
   textField: {
      margin: 0,
   },
   container: {
      display: 'flex',
      justifyContent: 'space-between',
   },
   image: {
      width: 100
   },
   error: {
      fontSize: 12,
      textAlign: 'center',
      color: 'red'
   }
}

const SoldProduct = ({
   product,
   handleToogleModal,
   handleChangeText,
   unitToSell,
   handleSellProduct,
   showError,
   classes
}) => {
   return (
      <div>
         <div className={classes.container}>
            <div>
               <Typography variant="body1" component="span"><strong>Producto:</strong> {product.name}</Typography>
               <Typography variant="body1" component="span"><strong>Unidades disponibles:</strong> {product.quantityEntry}</Typography>
               <Typography variant="body1" component="span"><strong>Valor unidad:</strong> $ {product.value.toLocaleString()}</Typography>
               <TextField
                  id="units-sell"
                  label="Unidades a remover"
                  className={classes.textField}
                  value={unitToSell}
                  onChange={handleChangeText()}
                  type="number"
                  onInput={(e) => {
                     e.target.value = Math.max(0, parseInt(e.target.value)).toString().slice(0, 3)
                  }}
               />
               <Typography variant="subtitle1" component="h1"><strong>Total a :</strong> $ {(product.value * unitToSell).toLocaleString()}</Typography>
            </div>
            <div>
               <img src={product.image} alt={product.name} className={classes.image} />
            </div>
         </div>
         {showError && <span className={classes.error}>Las unidades a remover no pueden se mayores a las unidades existentes.</span>}
         <div className={classes.actions}>
            <Button size="small" color="secondary" onClick={() => handleToogleModal(false)}> Cancelar </Button>
            <Button size="small" color="primary" onClick={() => handleSellProduct()} disabled={!unitToSell}> Remover </Button>
         </div>
      </div>
   )
}

export default withStyles(styles)(SoldProduct) 
