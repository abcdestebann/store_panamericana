import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
   root: {
      width: '100%',
      marginTop: 10,
   },
   heading: {
      fontSize: theme.typography.pxToRem(17),
      fontWeight: '400',
   },
});

const SectionPorudcts = ({ name, children, classes }) => {
   return (
      <div className={classes.root}>
         <ExpansionPanel defaultExpanded={name === 'Frutas'}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
               <Typography className={classes.heading}>{name}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
               {children}
            </ExpansionPanelDetails>
         </ExpansionPanel>
      </div>
   )
}

export default withStyles(styles)(SectionPorudcts) 