import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import CardProduct from './CardProduct';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import SectionPorudcts from './SectionPorudcts';
import Modal from '../../../components/Modal';
import SoldProduct from './SoldProduct'
import Nothing from '../../../components/Nothing';

const styles = {
   contentCard: {
      display: 'flex',
      flexWrap: 'wrap',
   },
   contentAdd: {

   }
}

const LayoutProducts = ({
   handleAddProduct,
   categories,
   modalOpened,
   handleToogleModal,
   productSelected = {},
   handleChangeText,
   unitToSell,
   handleSellProduct,
   showError,
   handleShowProduct,
   classes
}) => {
   return (
      <div style={{ marginBottom: '2rem' }}>
         <div style={{ marginBottom: '1rem' }}>
            <Button
               variant="extendedFab"
               color="secondary"
               aria-label="Add"
               className={classes.button}
               onClick={() => handleAddProduct()}>
               <AddIcon />
               Agregar producto
               </Button>
         </div>
         {Object.keys(categories).length > 0
            ? <div className={classes.contentCard}>
               {Object.keys(categories).map((category, index) => (
                  <SectionPorudcts key={category} name={category}>
                     {categories[category].map((product, index) => (
                        <CardProduct
                           key={product._id}
                           product={product}
                           category={category}
                           index={index}
                           handleToogleModal={handleToogleModal}
                           handleShowProduct={handleShowProduct} />
                     ))}
                  </SectionPorudcts>
               ))}
            </div>
            : <Nothing />}

         <Modal open={modalOpened} handleToogleModal={handleToogleModal}>
            <SoldProduct
               product={productSelected}
               handleToogleModal={handleToogleModal}
               handleChangeText={handleChangeText}
               unitToSell={unitToSell}
               handleSellProduct={handleSellProduct}
               showError={showError}
            />
         </Modal>
      </div>
   )
}

export default withStyles(styles)(LayoutProducts) 