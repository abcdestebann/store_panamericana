import React, { Component } from 'react'

// COMPONENTS
import LayoutProducts from './components/LayoutProducts';

// ALERT
import swal from 'sweetalert2'

const product = {
   name: '',
   value: '',
   entryDate: '',
   expirationDate: '',
   section: '',
   category: '',
   image: '',
   quantityEntry: '',
}

export default class Products extends Component {

   state = {
      modalOpened: false,
      productSelected: {},
      indexProduct: '',
      category: '',
      unitToSell: '',
      showError: false,
   }

   /**
    * Toogle modal
    * 
    * @param {*} modalOpened
    * @param {*} [productSelected={}]
    */
   handleToogleModal = (modalOpened, productSelected = {}, index, category = '') => {
      this.setState({ modalOpened, productSelected, indexProduct: index, category })
   }

   /**
    * 
    *
    * @memberof Products
    */
   handleChangeText = name => event => {
      this.setState({ unitToSell: event.target.value, })
   };

   /**
    * Make process to sell product and after show a confirmation of the this process
    * 
    * @memberof Products
    */
   handleSellProduct = () => {
      const { unitToSell, productSelected, category, indexProduct } = this.state
      if (unitToSell <= productSelected.quantityEntry) {
         const { handleSellProduct } = this.props
         this.setState({ showError: false, unitToSell: '' })
         handleSellProduct(unitToSell, productSelected, category, indexProduct).then(() => {
            swal({
               title: '',
               text: `Se han removido ${unitToSell} unidades de ${productSelected.name} del almacén satisfactoriamente.`,
               type: 'success',
               confirmButtonText: 'Aceptar'
            })
            this.handleToogleModal(false)
         })
      } else this.setState({ showError: true })
   }

   handleAddProduct = () => {
      const { handleShowProduct } = this.props
      handleShowProduct(product)
   }

   /**
    *
    *
    * @param {*} product
    */
   handleShowProduct = (product, index, category) => {
      const { handleShowProduct } = this.props
      handleShowProduct(product, index, category)
   }


   /**
    *
    *
    * @returns
    * @memberof Products
    */
   render() {
      const { categories } = this.props
      return (
         <LayoutProducts
            categories={categories}
            handleAddProduct={this.handleAddProduct}
            handleShowProduct={this.handleShowProduct}
            modalOpened={this.state.modalOpened}
            handleToogleModal={this.handleToogleModal}
            productSelected={this.state.productSelected}
            handleChangeText={this.handleChangeText}
            unitToSell={this.state.unitToSell}
            handleSellProduct={this.handleSellProduct}
            showError={this.state.showError} />
      )
   }
}
