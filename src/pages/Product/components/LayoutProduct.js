import React from 'react'
import { withStyles } from '@material-ui/core'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid';
import ImageProduct from './ImageProduct';
import InputProduct from './InputProduct';
import PickerProduct from './PickerProduct';
import SelectProduct from './SelectProduct';

const styles = {
   grid: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
   },
   gridImage: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
   },
   actions: {
      justifyContent: 'center',
      marginTop: '3%'
   },
   error: {
      textAlign: 'center',
      color: 'red',
      fontSize: 12,
      marginTop: 30
   }
}

const validateChange = (product, auxProduct) => {
   if (product._id && JSON.stringify(product) === JSON.stringify(auxProduct)) return false
   else return true
}

const LayoutProduct = ({
   classes,
   sections,
   product,
   categories,
   onChangeText,
   handleSelectImage,
   handleValidateFields,
   showErrorValidate,
   handleGoBack,
   auxProduct
}) => {
   return (
      <Card>
         <Grid container spacing={24}>
            <Grid item xs={3} className={classes.gridImage}>
               <ImageProduct handleSelectImage={handleSelectImage} imagen={product.image} />
            </Grid>
            <Grid item xs={4} className={classes.grid}>
               <InputProduct
                  label="Nombre del producto"
                  value={product.name}
                  id="name-product"
                  name="name"
                  onChangetext={onChangeText} />
               <InputProduct
                  label="Valor del producto"
                  value={product.value}
                  id="value-product"
                  name="value"
                  onChangetext={onChangeText} />
               <InputProduct
                  label="Cantidad del producto"
                  value={product.quantityEntry}
                  id="quantity-product"
                  name="quantityEntry"
                  onChangetext={onChangeText} />
            </Grid>
            <Grid item xs={4} className={classes.grid}>
               <PickerProduct
                  name="expirationDate"
                  value={product.expirationDate}
                  onChangeText={onChangeText} />
               <SelectProduct
                  value={product.category}
                  name="category"
                  label="Categoria"
                  items={categories}
                  onChangeText={onChangeText} />
               <SelectProduct
                  value={product.section}
                  name="section"
                  label="Sección"
                  items={sections}
                  onChangeText={onChangeText} />
            </Grid>
         </Grid>
         {showErrorValidate && <div className={classes.error}>
            <span>Debes diligenciar todos los campos para agregar el producto*</span>
         </div>}
         <CardActions className={classes.actions}>
            <Button color="secondary" onClick={() => handleGoBack()}>{product._id ? 'Volver' : 'Cancelar'}</Button>
            {validateChange(product, auxProduct) && <Button onClick={() => handleValidateFields()} color="primary">{product._id ? 'Guardar' : 'Agregar'}</Button>}
         </CardActions>
      </Card>
   )
}

export default withStyles(styles)(LayoutProduct)
