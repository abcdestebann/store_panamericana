import React from 'react'
import { withStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button'
import Image from '@material-ui/icons/Image'

const styles = {
   image: {
      height: 150,
      width: '100%',
      padding: 5
   },
   container: {
      padding: 10,
      textAlign: 'center',
   },
   icon: {
      fontSize: '15vh',
      color: '#d8d7d7'
   }
}

const ImageProduct = ({
   imagen,
   classes,
   handleSelectImage
}) => {
   return (
      <div onClick={() => handleSelectImage()} className={classes.container}>
         {
            imagen
               ? <img src={imagen} alt="imagen" className={classes.image} />
               : <Image className={classes.icon} />
         }
         <Button color="primary" >{imagen ? 'Cambiar imagen' : 'Agregar imagen'}</Button>
      </div>
   )
}


export default withStyles(styles)(ImageProduct) 
