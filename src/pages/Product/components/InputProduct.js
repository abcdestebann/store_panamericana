import React from 'react'
import { withStyles } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'


const styles = {
   input: {
      width: '90%'
   }
}

const InputProduct = ({
   id,
   value,
   label,
   onChangetext = () => { },
   name,
   classes
}) => {
   return (
      <div>
         <TextField
            id={id}
            label={label}
            value={value}
            onChange={onChangetext(name)}
            type={id !== 'name-product' ? 'number' : 'text'}
            margin="normal"
            className={classes.input}
         />
      </div>
   )
}

export default withStyles(styles)(InputProduct) 
