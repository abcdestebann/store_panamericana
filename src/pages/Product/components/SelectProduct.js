import React from 'react'
import { withStyles } from '@material-ui/core';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

const auxItems = ['1', '2', '3']

const styles = {
   form: {
      width: '90%'
   }
}

const SelectProduct = ({
   value,
   name,
   onChangeText = () => { },
   items = auxItems,
   label,
   classes
}) => {
   return (
      <FormControl className={classes.form}>
         <InputLabel htmlFor={`${name}-select`}>{label}</InputLabel>
         <Select
            value={value}
            onChange={onChangeText(name)}
            inputProps={{
               name: name,
               id: `${name}-select`,
            }}>
            {items.map((item) => <MenuItem key={item.id} value={item.value}>{item.value}</MenuItem>)}
         </Select>
      </FormControl>
   )
}

export default withStyles(styles)(SelectProduct) 
