import React from 'react'
import { withStyles } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'

const styles = {
   input: {
      width: '90%'
   }
}

const PickerProduct = ({
   classes,
   value,
   name,
   onChangeText = () => { }
}) => {
   return (
      <TextField
         id="date"
         label="Fecha de expiración"
         type="date"
         value={value}
         className={classes.input}
         InputLabelProps={{
            shrink: true,
         }}
         onChange={onChangeText(name)}
         margin="normal"
      />
   )
}


export default withStyles(styles)(PickerProduct)