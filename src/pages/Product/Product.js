import React, { Component } from 'react'
import LayoutProduct from './components/LayoutProduct';

const SECTIONS = [
   { value: 'Sección 1', id: '1' },
   { value: 'Sección 2', id: '2' },
   { value: 'Sección 3', id: '3' },
   { value: 'Sección 4', id: '4' },
   { value: 'Sección 5', id: '5' },
   { value: 'Sección 6', id: '6' },
   { value: 'Sección 7', id: '7' },
   { value: 'Sección 8', id: '8' },
   { value: 'Sección 9', id: '9' },
   { value: 'Sección 10', id: '10' },
]

const CATEGORIES = [
   { value: 'Carnes', id: '1' },
   { value: 'Frutas', id: '2' },
   { value: 'Granos', id: '3' },
   { value: 'Lacteos', id: '4' },
   { value: 'Verduras', id: '5' },
]

export default class Product extends Component {

   state = {
      product: this.props.product,
      showErrorValidate: false
   }

   componentWillMount() {
      this.product = this.state.product
   }

   onChangeText = name => event => {
      const product = { ...this.state.product }
      product[name] = event.target.value
      this.setState({ product })
   }

   handleSelectImage = () => {
      this.inputImage = document.getElementById('input-image')
      this.inputImage.click()
   }

   updateImage = (event) => {
      if (this.inputImage.files && this.inputImage.files[0]) {
         const reader = new FileReader();
         reader.onload = (e) => {
            const product = { ...this.state.product }
            product['image'] = e.target.result
            this.setState({ product })
         };
         reader.readAsDataURL(this.inputImage.files[0]);
      }
   }

   handleValidateFields = () => {
      const { product } = this.state
      const { name, value, category, section, image, expirationDate, quantityEntry } = product
      if (name && value && category && section && image && expirationDate && quantityEntry) {
         this.setState({ showErrorValidate: false })
         product.entryDate = new Date().getTime()
         this.handleVerifyAction(product)
      } else this.setState({ showErrorValidate: true })
   }

   /**
    *
    *
    * @param {*} product
    */
   handleVerifyAction = (product) => {
      const { handleCreateProduct, handleUpdateProduct } = this.props
      if (product._id) handleUpdateProduct(product)
      else handleCreateProduct(product)
   }

   handleGoBack = () => {
      const { handleShowProduct } = this.props
      handleShowProduct('')
   }

   render() {
      console.log(this.state.product)
      return (
         <div>
            <LayoutProduct
               product={this.state.product}
               auxProduct={this.product}
               sections={SECTIONS}
               categories={CATEGORIES}
               onChangeText={this.onChangeText}
               handleSelectImage={this.handleSelectImage}
               handleValidateFields={this.handleValidateFields}
               showErrorValidate={this.state.showErrorValidate}
               handleGoBack={this.handleGoBack} />
            <input id="input-image" style={{ display: 'none' }} type='file' onChange={this.updateImage} />
         </div>
      )
   }
}
