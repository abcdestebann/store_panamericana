import React from 'react'
import { Bar } from 'react-chartjs-2'

const serailizeData = (data) => {
   const labels = data.map(a => a.category);
   const newData = data.map(a => a.quantity)
   return {
      labels,
      datasets: [{
         label: 'Productos',
         data: newData,
         backgroundColor: [
            'rgba(239, 83, 80, 0.7)',
            'rgba(66, 165, 245, 0.7)',
            'rgba(255, 206, 86, 0.7)',
            'rgba(75, 192, 192, 0.7)',
            'rgba(153, 102, 255, 0.7)',
            'rgba(255, 159, 64, 0.7)'
         ],
         borderColor: [
            '#EF5350',
            '#42A5F5',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
         ],
         borderWidth: 2
      }]
   }
}

const ChartBar = ({ data }) => {
   return (
      <Bar
         data={serailizeData(data)}
         options={{
            maintainAspectRatio: false,
            scales: {
               yAxes: [{
                  ticks: {
                     beginAtZero: true,
                     userCallback: (label) => { if (Math.floor(label) === label) return label },
                  }
               }],
            },
         }}
         width={10} height={200} />
   )
}

export default ChartBar
