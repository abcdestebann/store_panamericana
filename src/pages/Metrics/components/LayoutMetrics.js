import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import ChartBar from './ChartBar';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import ChartDoughnut from './ChartDoughnut';

import ReactExport from 'react-data-export';
import Button from '@material-ui/core/Button';

const { ExcelFile } = ReactExport
const { ExcelSheet, ExcelColumn } = ReactExport.ExcelFile


const styles = {
   card: {
      minWidth: '40%',
      marginRight: 10,
      padding: 10,
   },
   headerCard: {
      textAlign: 'center'
   },
   divider: {
      margin: '20px 0',
   },
   container: {
      marginBottom: '2rem',
      display: 'flex',
      justifyContent: 'space-around',
      alignItems: 'center',
   }
};



const LayoutMetrics = ({ classes, dataBarMetrics, dataDoughnutMetrics }) => {
   return (
      <div>
         <div className={classes.container}>
            <Card className={classes.card}>
               <CardHeader
                  className={classes.headerCard}
                  titleTypographyProps={{ variant: 'subtitle2' }}
                  title="Cantidad de productos existentes por categoría" />
               <ChartBar data={dataBarMetrics} />
            </Card>
            <Card className={classes.card}>
               <CardHeader
                  className={classes.headerCard}
                  titleTypographyProps={{ variant: 'subtitle2' }}
                  title="Cantidad de productos vendidos por categoría" />
               <ChartDoughnut data={dataDoughnutMetrics} />
            </Card>
         </div>
            <ExcelFile filename="Reporte" element={<Button color="primary">Descargar Reporte</Button>}>

               <ExcelSheet data={dataBarMetrics} name="Productos existentes">
                  <ExcelColumn label="Categoria" value={(col) => col.category} />
                  <ExcelColumn label="Productos" value={(col) => col.quantity} />
               </ExcelSheet>

               <ExcelSheet data={dataDoughnutMetrics} name="Productos Vendidos">
               <ExcelColumn label="Categoria" value={(col) => col.category} />
               <ExcelColumn label="Productos" value={(col) => col.quantity} />
            </ExcelSheet>

         </ExcelFile>
      </div>
   )
}

export default withStyles(styles)(LayoutMetrics) 
