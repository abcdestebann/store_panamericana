import React, { Component } from 'react'
import LayoutMetrics from './components/LayoutMetrics';

class Metrics extends Component {
   render() {
      const { dataBarMetrics, dataDoughnutMetrics, generateReport } = this.props
      return (
         <div>
            <LayoutMetrics
               dataBarMetrics={dataBarMetrics}
               dataDoughnutMetrics={dataDoughnutMetrics}
               generateReport={generateReport} />
         </div>
      )
   }
}

export default Metrics