import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress';
import { Typography, withStyles } from '@material-ui/core';

const styles = {
   container: {
      height: '90vh',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
   }
}

const Loader = ({ title, classes }) => {
   return (
      <div className={classes.container}>
         <CircularProgress />
         <Typography variant="subtitle2" >{title}</Typography>
      </div>
   )
}

export default withStyles(styles)(Loader) 