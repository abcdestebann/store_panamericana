import React from 'react'
import { withStyles } from '@material-ui/core';

const styles = {
   container: {
      textAlign: 'center'
   },
   alerta: {
      width: '12%',
      marginTop: '40px',
   }
}

const Nothing = ({ classes }) => {
   return (
      <div className={classes.container}>
         <img className={classes.alerta} src="https://image.ibb.co/mXUKjf/alerta.png" alt="Alerta" />
         <p>¡UPS! No hay productos creados que podamos mostrar</p>
      </div>
   )
}

export default withStyles(styles)(Nothing) 