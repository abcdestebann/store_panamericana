import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

const styles = theme => ({
   container: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
   },
   paper: {
      position: 'absolute',
      alignSelf: 'center',
      width: theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
   },
});

const ModalComponent = ({ open, handleToogleModal, children, classes }) => {
   return (
      <Modal
         aria-labelledby="simple-modal-title"
         aria-describedby="simple-modal-description"
         open={open}
         onClose={() => handleToogleModal(false)}
         classes={{ root: classes.container }} >
         <div className={classes.paper}>
            {children}
         </div>
      </Modal>
   )
}

export default withStyles(styles)(ModalComponent) 
