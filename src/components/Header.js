import React from 'react'

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Typography from '@material-ui/core/Typography';

const Header = ({ classes }) => {
   return (
      <AppBar position="fixed" className={classes.appBar}>
         <Toolbar style={{ justifyContent: 'space-between' }}>
            <Typography variant="h6" color="inherit" noWrap> Almacén </Typography>
            <IconButton color="inherit">
               <AccountCircle />
            </IconButton>
         </Toolbar>
      </AppBar>
   )
}


export default Header