import React from 'react'

import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';


const Sidebar = ({ classes, sections, currentSection, handleSetSection }) => {
   return (
      <Drawer
         className={classes.drawer}
         variant="permanent"
         classes={{
            paper: classes.drawerPaper,
         }}
         anchor="left"
      >
         <div style={{ padding: '10%' }} className={classes.toolbar}>
            <img className={classes.logo} src="https://firebasestorage.googleapis.com/v0/b/instagram-clone-c5dfa.appspot.com/o/MocksWarehouse%20(1).png?alt=media&token=90c098b8-00c1-494b-bf6b-09b513bdf164" alt="logo" />
         </div>
         <Divider />
         <List>
            {sections.map((item, index) => (
               <ListItem
                  button
                  key={item.name}
                  selected={currentSection === item.name}
                  onClick={() => handleSetSection(item.name)}>
                  <ListItemIcon>{item.icon(currentSection === item.name ? 'secondary' : 'action')}</ListItemIcon>
                  <ListItemText primary={item.name} />
               </ListItem>
            ))}
         </List>
         <Divider />
      </Drawer>
   )
}

export default Sidebar