import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import Header from './Header';
import Sidebar from './Sidebar';

const drawerWidth = 240;

const styles = theme => ({
   root: {
      display: 'flex',
   },
   appBar: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
   },
   drawer: {
      width: drawerWidth,
      flexShrink: 0,
   },
   drawerPaper: {
      width: drawerWidth,
   },
   toolbar: theme.mixins.toolbar,
   content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      padding: theme.spacing.unit * 3,
      height: '100vh',
   },
   logo: {
      height: '100%',
      width: '100%',
   },   
});

const Layout = ({ classes, children, sections, currentSection, handleSetSection }) => {
   return (
      <div className={classes.root}>
         <Header classes={classes} />
         <Sidebar
            classes={classes}
            sections={sections}
            currentSection={currentSection}
            handleSetSection={handleSetSection} />
         <main className={classes.content}>
            <div className={classes.toolbar} />
            {children}
         </main>
      </div>
   )
}

export default withStyles(styles)(Layout)