export const API = "https://almacen-uni.herokuapp.com/api/"

export const ROUTES_PRODUCT = {
   GET: `${API}/product/get`,
   GET_ALL: `${API}/product/getAll`,
   CREATE: `${API}/product/create`,
   UPDATE: `${API}/product/update`,
   DELETE: `${API}/product/delete`,
}