export default {
  palette: {
    primary: {
      main: '#2196f3',
    },
    secondary: {
      main: '#ff5722'
    },
    error: {
      main: '#F44336'
    },
    action: {
      main: 'red'
    }
  },
  typography: {
    useNextVariants: true,
  },
}