import { ROUTES_PRODUCT } from '../config/index'
import axios from 'axios'


/**
 *
 *
 * @export
 * @returns
 */
export async function getAllProducts() {
   const { GET_ALL } = ROUTES_PRODUCT
   try {
      const { data } = await axios.get(GET_ALL)
      const { products } = data
      return products
   } catch (error) {
      console.log(error)
      throw new Error(error)
   }
}

/**
 *
 *
 * @export
 * @param {*} id
 * @returns
 */
export async function getProduct(id) {
   const { GET } = ROUTES_PRODUCT
   try {
      const { data } = await axios.get(`${GET}/${id}`)
      const { product } = data
      return product
   } catch (error) {
      console.log(error)
      throw new Error(error)
   }
}

/**
 *
 *
 * @export
 * @param {*} data
 * @returns
 */
export async function createProduct(dataProduct) {
   const { CREATE } = ROUTES_PRODUCT
   try {
      const { data } = await axios.post(CREATE, dataProduct)
      return data
   } catch (error) {
      console.log(error)
      throw new Error(error)
   }
}

/**
 *
 *
 * @export
 * @param {*} id
 * @param {*} dataProduct
 * @returns
 */
export async function updateProduct(id, dataProduct) {
   const { UPDATE } = ROUTES_PRODUCT
   try {
      const { data } = await axios.put(`${UPDATE}/${id}`, dataProduct)
      return data
   } catch (error) {
      console.log(error)
      throw new Error(error)
   }
}

/**
 *
 *
 * @export
 * @param {*} id
 * @returns
 */
export async function deleteProduct(id) {
   const { DELETE } = ROUTES_PRODUCT
   try {
      const { data } = await axios.delete(`${DELETE}/${id}`)
      return data
   } catch (error) {
      console.log(error)
      throw new Error(error)
   }
}