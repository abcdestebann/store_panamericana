import React, { Component } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

// THEME
import configTheme from './theme'


// API
import { getAllProducts, updateProduct, createProduct } from './api/product'

// COMPONENTS
import Layout from './components/Layout'
import Dashboard from '@material-ui/icons/Dashboard';
import BarChart from '@material-ui/icons/BarChart';
import Metrics from './pages/Metrics/Metrics';
import Product from './pages/Product/Product';
import Products from './pages/Products/Products';
import Loader from './components/Loader';
import Nothing from './components/Nothing';

const theme = createMuiTheme(configTheme);

const sections = [{
  name: 'Productos',
  icon: (color) => (<Dashboard color={color} />),
}, {
  name: 'Métricas',
  icon: (color) => (<BarChart color={color} />),
}]

class App extends Component {

  state = {
    sections,
    currentSection: sections[0].name,
    categories: {},
    products: [],
    product: '',
    dataBarMetrics: [],
    dataDoughnutMetrics: [],
    currentProduct: null,
    currentCategory: null,
    showLoading: false
  }

  async componentDidMount() {
    this.setState({ showLoading: true })
    const products = await getAllProducts() // Request API to fetch products
    console.log(products)
    this.setDataCategories(products)
    this.setState({ products, showLoading: false })
  }

  /**
   *  Create categories by product
   * 
   * @param {*} products
   */
  setDataCategories = (products) => {
    const categories = {}
    products.forEach(product => {
      if (categories[product.category]) categories[product.category] = [...categories[product.category], product]
      else categories[product.category] = [product]
    });
    this.setDataBarMetrics(categories)
    this.setDataDoughnutMetrics(categories)
    this.setState({ categories })
  }


  /**
   * Create data necessary to sho in a chart bar
   * 
   * @param {*} categories
   */
  setDataBarMetrics = (categories) => {
    const dataBarMetrics = []
    Object.keys(categories).forEach((category) => {
      const quantity = categories[category].map((product) => Number(product.quantityEntry)).reduce((accumulator, currentValue) => accumulator + currentValue)
      dataBarMetrics.push({ category, quantity })
    })
    this.setState({ dataBarMetrics })
  }

  /**
   * Create data necessary to sho in a chart doughnut
   *
   * @memberof App
   */
  setDataDoughnutMetrics = (categories) => {
    const dataDoughnutMetrics = []
    Object.keys(categories).forEach((category) => {
      const quantity = categories[category].map((product) => Number(product.sold)).reduce((accumulator, currentValue) => accumulator + currentValue)
      dataDoughnutMetrics.push({ category, quantity })
    })
    this.setState({ dataDoughnutMetrics })
  }

  /**
   * Set current section selected by user
   *
   * @param {*} currentSection
   */
  handleSetSection = (currentSection) => {
    this.setState({ currentSection, product: '' })
  }

  /**
   * Set prodoct to change the view and show it 
   *
   * @param {*} product
   */
  handleShowProduct = (product, index, category) => {
    console.log(index, category)
    this.setState({ product, currentProduct: index || null, currentCategory: category })
  }

  /**
   * Make simulation of selling sned an update to the API
   *
   * @param {*} unitToSell
   * @param {*} productSelected
   * @returns
   */
  handleSellProduct = async (unitToSell, productSelected, category, indexProduct) => {
    if (unitToSell <= productSelected.quantityEntry) {
      this.setState({ showError: false })
      const product = {
        ...productSelected,
        quantityEntry: productSelected.quantityEntry - Number(unitToSell),
        sold: productSelected.sold ? Number(unitToSell) + Number(productSelected.sold) : Number(unitToSell)
      }
      const result = await updateProduct(productSelected._id, product)
      this.reserializeProducts(product, category, indexProduct)
      return result
    } else this.setState({ showError: true })
  }

  /**
   * Re serialize categories with the change made in the product that was selling, recalculate data to show in charts
   *
   * @param {*} product
   * @param {*} category
   * @param {*} indexProduct
   */
  reserializeProducts = (product, category, indexProduct) => {
    const categories = { ...this.state.categories }
    categories[category].splice(indexProduct, 1, product)
    this.setDataBarMetrics(categories)
    this.setDataDoughnutMetrics(categories)
    this.setState({ categories })
  }


  /**
   *
   *
   * @param {*} product
   */
  handleCreateProduct = async (product) => {
    const products = [...this.state.products]
    const { productStored } = await createProduct(product)
    products.push(productStored)
    this.setState({ products, product: '' })
    this.setDataCategories(products)
  }

  /**
   *
   *
   * @param {*} product
   * @param {*} index
   */
  handleUpdateProduct = async (product) => {
    const { currentProduct, currentCategory } = this.state
    await updateProduct(product._id, product)
    this.reserializeProducts(product, currentCategory, currentProduct)
    this.setState({ product: '' })
  }

  /**
   *
   *
   * @returns
   * @memberof App
   */
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Layout
          sections={this.state.sections}
          currentSection={this.state.currentSection}
          handleSetSection={this.handleSetSection}>
          {
            this.state.products.length > 0 || !this.state.showLoading
              ? <div>
                {!this.state.product && this.state.currentSection === 'Productos' && <Products
                  categories={this.state.categories}
                  handleShowProduct={this.handleShowProduct}
                  handleSellProduct={this.handleSellProduct} />}

                {this.state.product && <Product
                  product={this.state.product}
                  handleCreateProduct={this.handleCreateProduct}
                  handleUpdateProduct={this.handleUpdateProduct}
                  handleShowProduct={this.handleShowProduct} />}

                {!this.state.product && this.state.currentSection === 'Métricas' && <Metrics
                  dataBarMetrics={this.state.dataBarMetrics}
                  dataDoughnutMetrics={this.state.dataDoughnutMetrics} />}
              </div>
              : <Loader title="Cargando productos" />
          }
        </Layout>
      </MuiThemeProvider>
    );
  }
}

export default App;
